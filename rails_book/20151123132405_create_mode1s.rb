class CreateMode1s < ActiveRecord::Migration
  def change
    create_table :mode1s do |t|
      t.string :data
      t.timestamps null: false
    end
  end
end

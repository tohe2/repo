#!/bin/sh
sudo mkdir -p /vagrant/hostinfo/db
echo "production:"                                                  | tee    database.yml
echo "  adapter: sqlite3"                                           | tee -a database.yml
echo "  database: /vagrant/hostinfo/db/redmine.sqlite"              | tee -a database.yml
echo "  timeout: 5000"                                              | tee -a database.yml
echo "production:"                                                  | tee    configuration.yml
echo "  email_delivery_method: :smtp"                               | tee -a configuration.yml
echo "  smtp_settings:"                                             | tee -a configuration.yml
echo "    address: localhost"                                       | tee -a configuration.yml
echo "    port: 25"                                                 | tee -a configuration.yml
echo "    domain: example.com"                                      | tee -a configuration.yml
echo "  rmagick_font_path: /usr/share/fonts/ipa-pgothic/ipagp.ttf"  | tee -a configuration.yml
sudo yum groupinstall 'Development Tools' -y
sudo yum install openssl-devel readline-devel zlib-devel curl-devel libyaml-devel libffi-devel ImageMagick ImageMagick-devel ipa-pgothic-fonts openssl-devel readline-devel zlib-devel sqlite-devel git -y
sudo svn co http://svn.redmine.org/redmine/branches/3.1-stable /var/lib/redmine
sudo chown -R vagrant:vagrant /var/lib/redmine
cp database.yml      /var/lib/redmine/config/
cp configuration.yml /var/lib/redmine/config/
git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' | tee ~/.bash_profile
echo 'eval "$(rbenv init -)"' | tee -a ~/.bash_profile
source ~/.bash_profile
cd /var/lib/redmine
rbenv install -v 2.2.3
rbenv local 2.2.3
gem install bundler
bundle install --without development test --path vendor/bundle
bundle exec rake generate_secret_token
RAILS_ENV=production bundle exec rake db:migrate
RAILS_ENV=production REDMINE_LANG=ja bundle exec rake redmine:load_default_data
RAILS_ENV=production REDMINE_LANG=ja bundle exec rails server -b 0.0.0.0

